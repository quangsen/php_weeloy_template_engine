module.exports = {
    app_js: {
        files: ['angular/tplapp/**/*.js'],
        tasks: [
            'jshint:app',
            'browserify'
        ],
        options: {
            spawn: false
        }
    },

    admin_js: {
        files: ['angular/tpladmin/**/*.js'],
        tasks: [
            'jshint:admin',
            'browserify:admin'
        ],
        options: {
            spawn: false
        }
    },

    app_template: {
        files: [
            'angular/tplapp/**/*.tpl.html'
        ],
        tasks: [
            'html2js:app',
            'browserify'
        ],
        options: {
            spawn: false
        }
    },

    admin_template: {
        files: [
            'angular/tpladmin/**/*.tpl.html'
        ],
        tasks: [
            'html2js:admin',
            'browserify:admin'
        ],
        options: {
            spawn: false
        }
    },
    common_sass: {
        files: [
            'public/css/common/common.scss'
        ],
        tasks: [
            'sass:common',
            'cssmin'
        ],
        options: {
            spawn: false
        }
    },
};
