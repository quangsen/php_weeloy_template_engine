module.exports = {
    options: {
        shorthandCompacting: false,
        roundingPrecision: -1
    },
    target: {
        files: {
            'public/css/tplapp.min.css': [
                'bower_components/angular-notify/angular-notify.css',
                'public/css/common.css',
            ],
            'public/css/tpladmin.min.css': [
                'bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
                'bower_components/bootstrap/dist/css/bootstrap.min.css',
                'bower_components/angular-notify/dist/angular-notify.min.css',
                'public/css/common/common.css',
            ]
        }
    }
}
