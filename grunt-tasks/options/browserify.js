module.exports = {
    app: {
        src: 'angular/tplapp/app.js',
        dest: 'public/js/tplapp.js'
    },
    admin: {
        src: 'angular/tpladmin/app.js',
        dest: 'public/js/tpladmin.js'
    }
};
