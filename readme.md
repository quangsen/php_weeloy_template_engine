#### 1. Create /app/config.php same as /app/config.example.php
```
<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Application Domains
    |--------------------------------------------------------------------------
    |
    | Array of all domain with restaurant ID and template name
    | @params string domain => domain of application
    |   string base_path => base path of application
    |   string template_name => the name of template will be display
    |   string restaurant_id => the id of restaurant will be display
    |   string default_language => the name of language will be display as default
    |   string facebook_app_id => facebook app id
    |   string customize_filename => name of json file to store all customize of that domain, each template has a folder /assets/customize/{customize_foldername}/{customize_filename}.json
    |   string customize_foldername => name of folder to store all customize of that domain, each template has a folder /assets/customize/{customize_foldername}
    |   string language_folder => name of folder to store all text of that domain with many language, each template has a folder /assets/languages/{language_folder}/{language_name}.json
     */
    'domains'       => [
        'bamboo.dev'   => [
            'base_path'            => '/',
            'template_name'        => 'bamboo',
            'restaurant_id'        => 'SG_SG_R_31BarKitchen',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'bamboo_dev',
            'customize_foldername' => 'bamboo_dev',
            'language_folder'      => 'bamboo_dev',
        ],
        'igloo.dev'    => [
            'base_path'            => '/',
            'template_name'        => 'igloo',
            'restaurant_id'        => 'SG_SG_R_31BarKitchen',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'customize',
            'customize_foldername' => 'igloo_dev',
            'language_folder'      => 'igloo_dev',
        ],
        'bancroft.dev' => [
            'base_path'            => '/',
            'template_name'        => 'bancroft',
            'restaurant_id'        => 'SG_SG_R_31BarKitchen',
            'default_language'     => 'en',
            'facebook_app_id'      => '',
            'customize_filename'   => 'customize',
            'customize_foldername' => 'bancroft_dev',
            'language_folder'      => 'bancroft_dev',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Base API URL
    |--------------------------------------------------------------------------
    |
    | the URL to retrieve all data
    |
     */
    'base_api_url'  => 'http://weeloy.dev/api',
    /*
    |--------------------------------------------------------------------------
    | Base Book URL
    |--------------------------------------------------------------------------
    |
    | the URL for booking form
    |
     */
    'base_book_url' => 'http://weeloy.dev',
];

?>
```
###$ 2. Run command composer install in template-engine
```
$ composer install
```