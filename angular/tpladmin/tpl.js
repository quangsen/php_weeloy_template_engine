angular.module('tpladmin.template', ['../angular/tpladmin/components/admin_customize/admin_customize.tpl.html', '../angular/tpladmin/components/admin_homepage/admin_homepage.tpl.html', '../angular/tpladmin/components/admin_language/admin_language.tpl.html', '../angular/tpladmin/components/admin_language/create/create.tpl.html', '../angular/tpladmin/components/admin_language/edit/edit.tpl.html', '../angular/tpladmin/components/admin_social/admin_social.tpl.html', '../angular/tpladmin/components/login/login.tpl.html', '../angular/tpladmin/components/profile/profile.tpl.html', '../angular/tpladmin/partials/customize.tpl.html', '../angular/tpladmin/partials/editLanguageFile.tpl.html', '../angular/tpladmin/partials/editLanguageFileInput.tpl.html']);

angular.module("../angular/tpladmin/components/admin_customize/admin_customize.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/components/admin_customize/admin_customize.tpl.html",
    "<div id=\"custom-page\">\n" +
    "    <div class=\"col-xs-12 col-sm-4 col-md-3\" ng-if=\"page != 'other'\">\n" +
    "        <h4>Enable/Disable</h4>\n" +
    "        <div class=\"switch\">\n" +
    "            <p class=\"title\">Enable menu for: {{page}}</p>\n" +
    "            <div class=\"icon\">\n" +
    "                <button class=\"switch-icon\" ng-class=\"pages[page].isEnable() ? 'switch-icon-on': 'switch-icon-off'\" ng-click=\"changePageStatus(page)\"></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"switch\" ng-repeat=\"section in pages[page].getSections()\">\n" +
    "            <p class=\"title\" ng-bind=\"section.name\"></p>\n" +
    "            <div class=\"icon\">\n" +
    "                <button class=\"switch-icon\" ng-class=\"pages[page].isEnableSection(section.id) ? 'switch-icon-on': 'switch-icon-off'\" ng-click=\"changeSectionStatus(page, section)\"></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"col-xs-12 col-sm-4 col-md-3\">\n" +
    "        <h4>Customize</h4>\n" +
    "        <div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">\n" +
    "            <div class=\"panel panel-default\" ng-repeat=\"item in customize\" ng-show=\"item.getPage() == page\">\n" +
    "                <div customize-item=\"item\" customize-item-id=\"$index\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/components/admin_homepage/admin_homepage.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/components/admin_homepage/admin_homepage.tpl.html",
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi totam quisquam iste, earum dicta modi voluptas ipsam deleniti beatae officiis quia ducimus unde mollitia molestiae tempore qui hic dolorem, doloremque maiores eius numquam cumque odit. At sapiente vel quibusdam tenetur molestiae placeat impedit, ex aliquid ipsum, architecto eligendi nam, accusamus fugiat, distinctio iste. Harum quas atque quos laborum eos magni reprehenderit fuga, eius consequuntur, unde velit optio qui reiciendis. Facilis sed officia, architecto consectetur earum iusto voluptatibus, aliquid, mollitia veniam id vitae a itaque. Modi iure, ab et id esse tempore omnis nulla ex temporibus voluptas? Quae omnis aperiam quos!");
}]);

angular.module("../angular/tpladmin/components/admin_language/admin_language.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/components/admin_language/admin_language.tpl.html",
    "<div class=\"col-xs-12 col-sm-4 col-md-3\">\n" +
    "    <ul class=\"list-group\">\n" +
    "        <a class=\"list-group-item\" ui-sref-active=\"active\" ui-sref=\"language.create\">create new</a>\n" +
    "        <a ng-repeat=\"file in LanguageFiles\" class=\"list-group-item\" ui-sref-active=\"active\" ui-sref=\"language.edit({filename: file})\" ng-bind=\"file\"></a>\n" +
    "    </ul>\n" +
    "</div>\n" +
    "<div class=\"col-xs-12 col-sm-8 col-md-9\">\n" +
    "	<div ui-view></div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/components/admin_language/create/create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/components/admin_language/create/create.tpl.html",
    "<div id=\"create-new-language\">\n" +
    "    <form name=\"CreateForm\" ng-submit=\"CreateForm.$valid && submit(name, language)\">\n" +
    "        <div class=\"form-group\">\n" +
    "            <label>Language name</label>\n" +
    "            <div class=\"input-group\">\n" +
    "                <input type=\"text\" class=\"form-control\" ng-model=\"name\" name=\"name\" aria-describedby=\"basic-addon2\" ng-trim=\"false\" ng-pattern=\"/^[a-z]*$/\" required>\n" +
    "                <span class=\"input-group-addon\" id=\"basic-addon2\">.json</span>\n" +
    "            </div>\n" +
    "            <div class=\"error\">\n" +
    "            	<span ng-show=\"CreateForm.name.$error.pattern\">Please enter a-z only</span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <edit-language-file data=\"language\"></edit-language-file>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <input type=\"submit\" class=\"btn btn-primary\" value=\"Save\">\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/components/admin_language/edit/edit.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/components/admin_language/edit/edit.tpl.html",
    "<div class=\"panel-group\" id=\"edit-language-file\" role=\"tablist\" aria-multiselectable=\"true\">\n" +
    "    <div class=\"form-group\">\n" +
    "        <edit-language-file data=\"language\"></edit-language-file>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "    	<button class=\"btn btn-primary\" ng-click=\"save()\">Save</button>\n" +
    "    	<button class=\"btn btn-default\" ng-click=\"cancel()\">Cancel</button>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/components/admin_social/admin_social.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/components/admin_social/admin_social.tpl.html",
    "<div id=\"social\">\n" +
    "    <div class=\"col-xs-12 col-sm-4\">\n" +
    "        <form name=\"SocialDataForm\" ng-submit=\"update(social)\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label>Facebook Page Url</label>\n" +
    "                <input type=\"url\" class=\"form-control\" ng-model=\"social.facebook_page\">\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label>Instagram Page Url</label>\n" +
    "                <input type=\"url\" class=\"form-control\" ng-model=\"social.instagram_url\">\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label>Instagram Wiget</label>\n" +
    "                <textarea style=\"min-height: 200px\" class=\"form-control\" ng-model=\"social.instagram_widget\"></textarea>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <input type=\"submit\" class=\"btn btn-default\" value=\"Update\">\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/components/login/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/components/login/login.tpl.html",
    "<div id=\"login\">\n" +
    "    <div class=\"col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4\">\n" +
    "        <h2>Login</h2>\n" +
    "        <form method=\"POST\" ng-submit=\"login(user)\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label>Email</label>\n" +
    "                <input type=\"email\" ng-model=\"user.email\" class=\"form-control\" required>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label>Password</label>\n" +
    "                <input type=\"password\" ng-model=\"user.password\" class=\"form-control\" required>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <input type=\"submit\" class=\"btn btn-primary\" value=\"Login\">\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/components/profile/profile.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/components/profile/profile.tpl.html",
    "<div id=\"update-password\">\n" +
    "    <div class=\"col-xs-12 col-sm-4 col-sm-offset-4\">\n" +
    "        <h2>Update password</h2>\n" +
    "        <form method=\"POST\" name=\"UpdatePasswordForm\" ng-submit=\"new_password == repeat_password && UpdatePasswordForm.$valid && update(old_password, new_password)\" novalidate>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label>Old password</label>\n" +
    "                <input type=\"password\" class=\"form-control\" name=\"old_password\" ng-model=\"old_password\" required>\n" +
    "                <div class=\"error\">\n" +
    "                    <span ng-show=\"UpdatePasswordForm.$submitted && UpdatePasswordForm.old_password.$error.required\">Please enter current password</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label>New password</label>\n" +
    "                <input type=\"password\" class=\"form-control\" name=\"new_password\" ng-model=\"new_password\" required>\n" +
    "                <div class=\"error\">\n" +
    "                    <span ng-show=\"UpdatePasswordForm.$submitted && UpdatePasswordForm.new_password.$error.required\">Please enter new password</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <label>Repeat password</label>\n" +
    "                <input type=\"password\" class=\"form-control\" name=\"repeat_password\" ng-model=\"repeat_password\" required>\n" +
    "                <div class=\"error\">\n" +
    "                    <span ng-show=\"UpdatePasswordForm.$submitted && UpdatePasswordForm.repeat_password.$error.required\">Please repeat new password</span>\n" +
    "                    <span ng-show=\"UpdatePasswordForm.$submitted && new_password != repeat_password\">repeat password does not match</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"form-group\">\n" +
    "                <input type=\"submit\" class=\"btn btn-default\" value=\"Update\" required>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/partials/customize.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/partials/customize.tpl.html",
    "<div class=\"panel-heading\" role=\"tab\" id=\"headingOne\">\n" +
    "    <h4 class=\"panel-title\">\n" +
    "        <p ng-click=\"collapse(customizeItemId)\" ng-bind=\"customizeItem.getTitle()\"></p>\n" +
    "    </h4>\n" +
    "</div>\n" +
    "<div id=\"customize-item-{{customizeItemId}}\" class=\"panel-collapse collapse\" aria-expanded=\"false\" role=\"tabpanel\">\n" +
    "    <div class=\"panel-body\">\n" +
    "        <!-- Type: text -->\n" +
    "        <div class=\"customize-attribute\" ng-repeat=\"(k, value) in customizeItem.variables\">\n" +
    "            <div ng-if=\"value.type == 'text'\" class=\"form-group\">\n" +
    "                <label ng-bind=\"value.title\"></label>\n" +
    "                <input type=\"text\" class=\"form-control\" ng-model=\"value.data\">\n" +
    "            </div>\n" +
    "            <!-- Type: image -->\n" +
    "            <div ng-if=\"value.type == 'image'\" class=\"form-group\">\n" +
    "                <label ng-bind=\"value.title\"></label>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"btn btn-default\" ngf-select=\"upload($file, customizeItem, k)\">Choose File</div>\n" +
    "                    <div class=\"btn btn-default\" ng-click=\"default(customizeItem, k)\">Default</div>\n" +
    "                    <div class=\"btn btn-default\" ng-click=\"none(customizeItem, k)\">None</div>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\" ng-show=\"value.data != ''\">\n" +
    "                    <img ng-src=\"{{value.data}}\" ng-show=\"value.data != 'none'\" class=\"img-responsive\" alt=\"\">\n" +
    "                    <div ng-show=\"value.data == 'none'\" class=\"text-center\">\n" +
    "                        <span style=\"font-size: 40px\" class=\"glyphicon glyphicon-ban-circle\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"progress\" ng-show=\"progressPercent | showProgressBar\">\n" +
    "                    <div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"{{progressPercent}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progressPercent}}%\">\n" +
    "                        <span class=\"sr-only\">{{progressPercent}}% Complete</span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <!-- Type: Select -->\n" +
    "            <div ng-if=\"value.type == 'select'\" class=\"form-group\">\n" +
    "                <label ng-bind=\"value.title\"></label>\n" +
    "                <select class=\"form-control\" ng-model=\"value.data\" ng-options=\"item.data as item.name for item in value.data_available\"></select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <button class=\"btn btn-default pull-right\" ng-click=\"cancel(customizeItemId)\">Cancel</button>\n" +
    "            <button class=\"btn btn-default pull-right\" ng-click=\"update(customizeItemId, customizeItem)\" style=\"margin-right: 5px;\">Save</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/partials/editLanguageFile.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/partials/editLanguageFile.tpl.html",
    "<div class=\"panel panel-default item\" ng-repeat=\"(key, value) in data\">\n" +
    "    <div class=\"panel-heading item-heading\" role=\"tab\">\n" +
    "        <h4 class=\"panel-title item-title\" ng-click=\"collapse($event)\" ng-bind=\"key\"></h4>\n" +
    "    </div>\n" +
    "    <div class=\"panel-collapse collapse\" role=\"tabpanel\">\n" +
    "        <div class=\"panel-body\" recursion=\"value\"></div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../angular/tpladmin/partials/editLanguageFileInput.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../angular/tpladmin/partials/editLanguageFileInput.tpl.html",
    "<div class=\"form-group\">\n" +
    "    <input type=\"text\" class=\"form-control\" ng-model=\"recursion\">\n" +
    "</div>\n" +
    "");
}]);
