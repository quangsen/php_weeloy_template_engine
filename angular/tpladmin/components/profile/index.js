(function(app) {
    app.controller('ProfileCtrl', ProfileCtrl);
    ProfileCtrl.$inject = ['$rootScope', '$scope', 'API', 'Notification'];

    function ProfileCtrl($rootScope, $scope, API, Notification) {
        $scope.update = function(old_password, new_password) {
            API.user.update($rootScope.user.id, old_password, new_password)
                .then(function(result) {
                    Notification.show('success', 'password was updated');
                })
                .catch(function(error) {
                    Notification.show('warning', error);
                });
        };
    }
    app.filter('json', function() {
        return function(object) {
            return JSON.stringify(object);
        };
    });
})(angular.module('admin.components.profile', []));
