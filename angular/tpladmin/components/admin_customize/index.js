(function(app) {
    app.controller('CustomizeCtrl', CustomizeCtrl);
    CustomizeCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', 'Notification'];

    function CustomizeCtrl($rootScope, $scope, $stateParams, API, Notification) {
        function loadCustomizeObject() {
            API.customize.get()
                .then(function(result) {
                    $scope.customize = result;
                })
                .catch(function(error) {
                    throw (error);
                });
        }
        loadCustomizeObject();
        /*
         * Global function to reload Customize Object
         */
        $rootScope.reloadCustomizeObject = function(customizeItemId) {
            API.customize.get()
                .then(function(result) {
                    $scope.customize[customizeItemId] = result[customizeItemId];
                })
                .catch(function(error) {
                    throw (error);
                });
        };

        /*
         * Get All Pages
         */
        $scope.page = $stateParams.page;

        /*
         * Change section status visible/hiden
         */
        $scope.changeSectionStatus = function(page, section) {
            API.customize.changePageSectionStatus(page, section)
                .then(function(response) {
                    section = response;
                    $rootScope.pages[page].sections.forEach(function(value, key) {
                        if (value.id == section.id) {
                            $scope.pages[page].sections[key] = section;
                        }
                    });
                })
                .catch(function(error) {
                    console.log(error);
                    Notification.show('warning', error.data.error);
                });
        };
        /*
         * Change page status visible/hiden
         */
        $scope.changePageStatus = function(page) {
            API.customize.changePageStatus(page)
                .then(function(response) {
                    $rootScope.pages[page] = response;
                })
                .catch(function(error) {
                    console.log(error);
                });
        };
    }
})(angular.module('admin.components.customize', []));
