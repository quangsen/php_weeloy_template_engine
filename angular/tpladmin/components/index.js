require('./admin_customize/');
require('./admin_language/');
require('./admin_social/');
require('./login/');
require('./profile/');
(function(app) {

})(angular.module('admin.components', [
    'admin.components.customize',
    'admin.components.language',
    'admin.components.social',
    'admin.components.login',
    'admin.components.profile'
]));
