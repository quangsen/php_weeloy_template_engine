(function(app) {
    app.directive('editLanguageFile', theDirective);
    theDirective.$inject = ['$rootScope', '$compile', 'API', 'Notification'];

    function theDirective($rootScope, $compile, API, Notification) {
        return {
            restrict: 'AE',
            templateUrl: '../angular/tpladmin/partials/editLanguageFile.tpl.html',
            scope: {
                data: '=',
            },
            link: function(scope, element, attrs) {
                $('.collapse').collapse('hide');
                scope.collapse = function(evt) {
                    $(evt.target).parent().next().collapse('toggle');
                };
            }
        };
    }

    app.directive('recursion', recursion);

    recursion.$inject = ['$rootScope', '$compile', '$parse', 'API'];

    function recursion($rootScope, $compile, $parse, API) {
        return {
            restrict: 'A',
            scope: {
                recursion: '=',
                key: '=',
            },
            link: function(scope, element, attrs) {
                var html;
                if (typeof scope.recursion === 'object') {
                    html = '<edit-language-file data="recursion"></edit-language-file>';
                } else {
                    html = '<div class="form-group">' +
                        '<input type="text" class="form-control" ng-model="recursion" ng-change="saveData(recursion)" ng-model-options="{ debounce: 1000 }">' +
                        '</div>';
                }
                var el = $compile(html)(scope);
                element.append(el);
                scope.saveData = function(recursion) {
                    var selector = [];
                    var ItemHeading = element.parent().prev();
                    while (ItemHeading.parent().parent().is('edit-language-file')) {
                        selector.unshift(ItemHeading.find('.item-title').text());
                        ItemHeading = ItemHeading.parent().parent().parent().parent().prev();
                    }
                    selector = selector.join('.');
                    selector = 'language' + '.' + selector;
                    var model = $parse(selector);
                    model.assign($rootScope, recursion);
                    console.log(selector);
                    console.log($rootScope.language);
                };
            }
        };
    }

    app.filter('isObject', function() {
        return function(value) {
            if (value === undefined) {
                return false;
            } else {
                if (typeof value === 'object') {
                    return true;
                }
            }
        };
    });

    app.factory('RecursionHelper', ['$compile', function($compile) {
        return {
            /**
             * Manually compiles the element, fixing the recursion loop.
             * @param element
             * @param [link] A post-link function, or an object with function(s) registered via pre and post properties.
             * @returns An object containing the linking functions.
             */
            compile: function(element, link) {
                // Normalize the link parameter
                if (angular.isFunction(link)) {
                    link = {
                        post: link
                    };
                }

                // Break the recursion loop by removing the contents
                var contents = element.contents().remove();
                var compiledContents;
                return {
                    pre: (link && link.pre) ? link.pre : null,
                    /**
                     * Compiles and re-adds the contents
                     */
                    post: function(scope, element) {
                        // Compile the contents
                        if (!compiledContents) {
                            compiledContents = $compile(contents);
                        }
                        // Re-add the compiled contents to the element
                        compiledContents(scope, function(clone) {
                            element.append(clone);
                        });

                        // Call the post-linking function, if any
                        if (link && link.post) {
                            link.post.apply(null, arguments);
                        }
                    }
                };
            }
        };
    }]);
})(angular.module('admin.directives.editLanguageFile', []));
