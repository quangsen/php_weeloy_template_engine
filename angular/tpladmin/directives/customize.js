(function(app) {
    app.directive('customizeItem', theDirective);
    theDirective.$inject = ['$rootScope', 'API', 'Notification'];

    function theDirective($rootScope, API, Notification) {
        return {
            restrict: 'AE',
            templateUrl: '../angular/tpladmin/partials/customize.tpl.html',
            scope: {
                customizeItem: '=',
                customizeItemId: '=',
            },
            link: function(scope, element, attrs) {
                $('.collapse').collapse('hide');
                scope.collapse = function(customizeItemId) {
                    $('#customize-item-' + customizeItemId).collapse('toggle');
                };
                /*
                 * Update customize
                 */
                scope.update = function(customizeItemId, customizeItem) {
                    var data = {
                        index: customizeItemId,
                        item: customizeItem,
                    };
                    API.customize.update(data)
                        .then(function(result) {
                            console.log(result);
                            Notification.show('success', 'Save Successfull');
                        })
                        .catch(function(error) {
                            Notification.show('warning', error);
                            throw (error);
                        });
                };
                scope.upload = function(file, customizeItem, key) {
                    API.customize.upload(file)
                        .then(function(url) {
                            customizeItem.variables[key].data = url;
                            Notification.show('success', 'Upload Complete');
                        })
                        .catch(function(error) {
                            Notification.show('warning', error);
                            throw (error);
                        });
                };
                $rootScope.$on('UploadProgressPercent', function(evt, percent) {
                    scope.progressPercent = percent;
                });
                /*
                 * Cancel
                 */
                scope.cancel = function(customizeItemId) {
                    $rootScope.reloadCustomizeObject(customizeItemId);
                };
                /*
                 * Make data is empty
                 */
                scope.default = function(customizeItem, key) {
                    customizeItem.variables[key].data = '';
                };
                /*
                 * Make data is none
                 */
                scope.none = function(customizeItem, key) {
                    customizeItem.variables[key].data = 'none';
                };
            }
        };
    }

    app.filter('showProgressBar', function() {
        return function(percent) {
            if (percent === null || percent === undefined || percent === 100) {
                return false;
            } else {
                return true;
            }
        };
    });
})(angular.module('admin.directives.customize', []));
