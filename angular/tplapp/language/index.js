require('angular-translate');
require('angular-translate-loader-static-files');
(function(app) {
    app.config(['$translateProvider', 'configProvider', function($translateProvider, configProvider) {
        $translateProvider.useSanitizeValueStrategy(null);
        var prefix = configProvider.Config.base_url + '/templates/' + configProvider.Config.template_name + '/assets/languages/' + configProvider.Config.language_folder + '/';
        $translateProvider.useStaticFilesLoader({
            prefix: prefix,
            suffix: '.json'
        });
        $translateProvider.preferredLanguage(configProvider.Config.default_language);
    }]);
})(angular.module('tplapp.language', ['pascalprecht.translate', 'tplapp.config']));
