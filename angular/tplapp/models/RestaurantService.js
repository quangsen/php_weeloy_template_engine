var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function RestaurantService(options) {
   	this.ID = null;
   	this.active = '';
   	this.category_id = '';
   	this.pico_categorie = '';
   	this.pico_service = '';
   	this.service = '';
   	this.categorie = '';

    BaseModel.call(this, options);
}

inherits(RestaurantService, BaseModel);

RestaurantService.prototype.getCategoryName = function() {
	return this.categorie;
};

RestaurantService.prototype.getCategoryId = function() {
	return this.categorie_id;
};

module.exports = RestaurantService;