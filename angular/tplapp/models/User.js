var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function User(options) {
    this.id = null;
    this.email = '';
    BaseModel.call(this, options);
}
inherits(User, BaseModel);

User.prototype.getId = function() {
    return this.id;
};

User.prototype.getEmail = function() {
    return this.email;
};

module.exports = User;
