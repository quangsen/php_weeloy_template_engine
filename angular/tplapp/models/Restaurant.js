var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Restaurant(options) {
    this.GPS = '';
    this.ID = null;
    this.email = '';
    this.tel = '';
    this.address = '';
    this.address1 = '';
    this.best_offer = {};
    this.book_button = {};
    this.city = '';
    this.chef = {};
    this.country = '';
    this.cuisine = [];
    this.currency = '';
    this.extraflag = '';
    this.hotelname = '';
    this.image = '';
    this.images = [];
    this.gallery = [];
    this.internal_path = '';
    this.internal_path = '';
    this.is_bookable = null;
    this.is_displayed = null;
    this.is_favorite = null;
    this.is_wheelable = null;
    this.likes = 0;
    this.logo = '';
    this.map = '';
    this.mealtype = [];
    this.morder = '';
    this.openhours = '';
    this.pricediner = '';
    this.pricelunch = '';
    this.pricing = '';
    this.rating = '';
    this.region = '';
    this.restaurant = '';
    this.status = '';
    this.title = '';
    this.wheel = '';
    this.wheelImageUrl = '';
    this.wheelvalue = '';
    this.zip = '';
    this.video = '';
    this.services = [];
    this.description = [];
    //this.descriptionWeb = '';
    this.chef_desc_website = '';
    this.menu = [];
    this.takeoutrestaurant = 0;
    this.gmapMarker = null;
    this.mediaServer = '//media.weeloy.com';

    BaseModel.call(this, options);
}

inherits(Restaurant, BaseModel);

Restaurant.prototype.getRestaurantID = function() {
    return this.restaurant;
};

Restaurant.prototype.setMediaServer = function(mediaServer) {
    this.mediaServer = mediaServer;
};

Restaurant.prototype.setMenu = function(val) {
    if (Array.isArray(val)) {
        this.menu = val;
    }
    return this;
};

Restaurant.prototype.getMenus = function() {
    return this.menu;
};

Restaurant.prototype.getImage = function(imageSizePath) {
    imageSizePath = imageSizePath || '/';
    var url = this.mediaServer + '/upload/restaurant/' + encodeURIComponent(this.restaurant) + imageSizePath + encodeURIComponent(this.image);
    return url;
};

Restaurant.prototype.getMenuImage = function(file, imageSizePath) {
    imageSizePath = imageSizePath || '/';
    var url = this.mediaServer + '/upload/restaurant/' + encodeURIComponent(this.restaurant) + imageSizePath + encodeURIComponent(file);
    return url;
};

Restaurant.prototype.getWheelImage = function(imageSizePath) {
    var url = this.mediaServer + '/upload/wheelvalues' + imageSizePath + 'wheelvalue_' + this.wheelvalue + '.png';
    return url;
};

Restaurant.prototype.getInternalPath = function() {
    var restaurant_details = this.restaurant.split('_');
    var city;
    switch (restaurant_details[1]) {
        case 'SG':
            city = 'singapore';
            break;
        case 'HK':
            city = 'hong-kong';
            break;
        case 'BK':
            city = 'bangkok';
            break;
        case 'PK':
            city = 'phuket';
            break;
        case 'KL':
            city = 'kuala-lumpur';
            break;
        case 'PR':
            city = 'paris';
            break;
        case 'SE':
            city = 'seoul';
            break;
        default:
            city = 'singapore';
            break;
    }
    var type = 'restaurant';
    var restaurant_name = this.restaurant.substr(8);
    restaurant_name = restaurant_name.replace(/_/g, '');
    restaurant_name = restaurant_name.replace(/([A-Z])/g, '-$1');
    restaurant_name = restaurant_name.replace(/[-]+/, '-');
    restaurant_name = restaurant_name.toLowerCase();
    if (restaurant_name.charAt(0) == '-') {
        restaurant_name = restaurant_name.substr(1);
    }

    var restaurant_url = type + '/' + city + '/' + restaurant_name;
    return restaurant_url;
};

Restaurant.prototype.getBannerImage = function(imageSizePath) {
    imageSizePath = imageSizePath || '/1440/';
    var banner = this.mediaServer + '/upload/restaurant/' + this.restaurant + imageSizePath + encodeURIComponent(this.images[0]);
    return banner;
};

Restaurant.prototype.bannerStyle = function(imageSizePath) {
    var style = {
        backgroundImage: 'url(\'' + this.getBannerImage(imageSizePath) + '\')',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    };
    return style;
};

Restaurant.prototype.getTitle = function() {
    return this.title;
};

Restaurant.prototype.getStatus = function() {
    return this.status;
};

Restaurant.prototype.getAddress = function() {
    return this.address;
};

Restaurant.prototype.getPhone = function() {
    return this.tel;
};

Restaurant.prototype.getEmail = function() {
    return this.email;
};

Restaurant.prototype.getZip = function() {
    return this.zip;
};

Restaurant.prototype.getCity = function() {
    return this.city;
};

Restaurant.prototype.getCountry = function() {
    return this.country;
};

Restaurant.prototype.getWheelValue = function() {
    return this.wheelvalue;
};

Restaurant.prototype.getLogoImage = function() {
    var logo = this.mediaServer + '/upload/restaurant/' + this.restaurant + '/' + encodeURIComponent(this.logo);
    return logo;
};

Restaurant.prototype.getCuisineAsArray = function() {
    return this.cuisine.split('|');
};

Restaurant.prototype.getWheelAsArray = function() {
    var wheel = this.wheel.split('|');
    wheel.splice(0, 1);
    return wheel;
};

Restaurant.prototype.getLatitude = function() {
    var GPS = this.GPS;
    GPS = GPS.split(',');
    return {
        lat: GPS[0],
        lng: GPS[1]
    };
};

Restaurant.prototype.getDescription = function() {
    return this.description;
};

Restaurant.prototype.getGallery = function(imageSizePath) {
    return this.gallery;
};

Restaurant.prototype.getWheelRewardImage = function() {
    var wheel = this.mediaServer + '/upload/restaurant/' + this.restaurant + '/wheel.png?time=55ea4bf6d05e6';
    return wheel;
};

Restaurant.prototype.getMarker = function() {
    return this.gmapMarker;
};

Restaurant.prototype.setMarker = function(marker) {
    this.gmapMarker = marker;
    return this;
};


Restaurant.prototype.setDescription = function(webrestodesc) {
    this.description = webrestodesc;
    return this;
};

Restaurant.prototype.setChefDescription = function(chefdesc){
    this.chef_desc_website = chefdesc;
    return this;
};

//Restaurant.prototype.setMarker = function(webchefdesc) {
//    this.chef.descwebchefdesc = webchefdesc;
//    return this;
//};

module.exports = Restaurant;
