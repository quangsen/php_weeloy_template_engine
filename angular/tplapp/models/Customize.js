var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Customize(options) {
    this.selector = '';
    this.title = '';
    this.page = '';
    this.type = '';
    this.attributes = [];
    this.variables = [];
    BaseModel.call(this, options);
}
inherits(Customize, BaseModel);

Customize.prototype.getSelector = function() {
    return this.selector;
};

Customize.prototype.getTitle = function() {
    return this.title;
};

Customize.prototype.getType = function() {
    return this.type;
};

Customize.prototype.getAttributes = function() {
    return this.attributes;
};

Customize.prototype.getPage = function() {
    return this.page;
};
module.exports = Customize;
