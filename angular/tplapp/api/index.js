require('./apiUrl');
require('./restaurant/');
require('./customize/');
require('./language/');
require('./social/');
require('./user/');
require('./common/');

(function(app) {
    app.factory('API', factory);
    factory.$inject = [
        'RestaurantService',
        'CustomizeService',
        'LanguageService',
        'SocialService',
        'UserService',
        'CommonService'
    ];

    function factory(
        RestaurantService,
        CustomizeService,
        LanguageService,
        SocialService,
        UserService,
        CommonService
    ) {
        var service = {
            restaurant: RestaurantService,
            customize: CustomizeService,
            language: LanguageService,
            social: SocialService,
            user: UserService,
            common: CommonService,
        };
        return service;
    }

})(angular.module('tplapp.api', [
    'tplapp.api.url',
    'tplapp.api.restaurant',
    'tplapp.api.customize',
    'tplapp.api.language',
    'tplapp.api.social',
    'tplapp.api.common',
    'app.api.user',
]));
