require('../config');

(function(app) {
    app.service('ApiUrl', ['Config', function(Config) {
        this.get = function(url) {
            return Config.base_api_url + url;
        };

        this.getApiInCurrentDomain = function(url) {
            if (Config.base_path === '/') {
                return Config.base_url + '/api' + url;
            } else {
                return Config.base_url + Config.base_path + '/api' + url;
            }
        };
    }]);
})(angular.module('tplapp.api.url', ['tplapp.config']));
