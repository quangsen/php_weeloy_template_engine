(function(app) {
    app.config(['$logProvider', '$provide', function($logProvider, $provide) {
        $logProvider.debugEnabled(true);

        $provide.decorator('$exceptionHandler', [
            '$log',
            '$delegate',
            '$injector',
            function($log, $delegate, $injector) {
                return function(exception) {
                    $log.debug('Exception handler:', exception);

                    //$delegate(exception, cause);
                    var $rootScope = $injector.get("$rootScope");
                    $rootScope.$broadcast('onError', exception);
                };
            }
        ]);
    }]);
    /*
     * Show Error message
     */
    app.run(errorHandler);
    errorHandler.$inject = ['$rootScope'];

    function errorHandler($rootScope) {
        $rootScope.$on('onError', function(e, err) {
            console.log(e, err);
            if (!$rootScope.hasError) {
                $rootScope.hasError = true;
            }
        });
    }
})(angular.module('tplapp.exception', []));
