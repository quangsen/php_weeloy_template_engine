<?php
namespace App\libs;

class Transform
{
    /**
     * convert string to function. ex: {{ restaurant.getTitle() }} => $class->restaurant->getTitle()
     * @param class $class, string $string
     * @return string with data
     */
    public static function convert($class, $string)
    {
        // find all function match: {function_name}({variable})
        $pattern_app_function = '/\{\{\s*(\S+)\([\"|\'|]*(\S*)[\"|\']*\)\s*\}\}/';
        // find all function match: {model}.{function_name}({variable})
        $pattern_restaurant_function = '/\{\{\s*((\S+)\.(\S+)\((\S*)\))\s*\}\}/';
        preg_match_all($pattern_restaurant_function, $string, $matches, PREG_SET_ORDER);
        if (isset($matches) && count($matches) > 0) {
            foreach ($matches as $value) {
                if (count($value) > 4) {
                    $replace_data = $class->{$value[2]}->{$value[3]}($value[4]);
                    $string       = str_replace($value[0], $replace_data, $string);
                }
            }
        }
        preg_match_all($pattern_app_function, $string, $matches, PREG_SET_ORDER);
        if (isset($matches) && count($matches) > 0) {
            foreach ($matches as $value) {
                if (count($value) > 2) {
                    $replace_data = $class->{$value[1]}($value[2]);
                    $string       = str_replace($value[0], $replace_data, $string);
                }
            }
        }
        return $string;
    }
}
