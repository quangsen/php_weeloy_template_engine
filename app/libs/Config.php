<?php

namespace App\libs;

use Exception;

class Config extends Exception
{
    private $config;
    public function __construct()
    {
        $this->config = $this->readData();
    }
    /**
     * Return value of config by a key
     * @param String $key
     * @return value in config file
     */
    public function get($key)
    {
        return $this->config[$key];
    }
    /**
     * Get App Config Data
     * @return Config Object
     */
    public function readData()
    {
        $config_dir = __DIR__ . '/../config.php';
        if (!file_exists($config_dir)) {
            throw new Exception("config file not found or not readable", 1);
        } else {
            $config        = require_once $config_dir;
            $domain_config = $config['domains'][$_SERVER['HTTP_HOST']];
            if (!isset($domain_config)) {
                throw new Exception("Can not find this domain in config file", 1);
            } else {
                $this->requireParams(['template_name', 'restaurant_id', 'default_language', 'base_path'], $domain_config);
                unset($config['domains']);
                $data = array_merge($config, $domain_config);
                return $data;
            }
        }
    }
    /**
     * throw error if a param was not required
     * @param Array $params, Array $config
     */
    public function requireParams($params, $config)
    {
        foreach ($params as $param) {
            if (!isset($config[$param])) {
                throw new Exception("Can not find \"" . $param . "\" in config file for this domain", 0);
            }
        }
    }
}
