<?php
namespace App\libs;

use Exception;

class File
{
    private $dir;
    public function __construct($dir = null)
    {
        if (isset($dir)) {
            $this->dir = $dir;
        }
    }

    /**
     * Gets the value of dir.
     *
     * @return mixed
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * Sets the value of dir.
     *
     * @param mixed $dir the dir
     *
     * @return self
     */
    public function setDir($dir)
    {
        $this->dir = $dir;

        return $this;
    }

    /**
     * check file exist
     *
     * @return boolean
     */
    public function isExist()
    {
        return file_exists($this->getDir());
    }

    /**
     * get file name
     *
     * @return string $name
     */
    public function getName()
    {
        return basename($this->getDir());
    }

    /**
     * read file
     *
     * @return mixed
     */
    public function read()
    {
        if ($this->isExist() == false) {
            throw new Exception("file doesn't exist", 0);
        } else {
            $data = file_get_contents($this->getDir());
            $data = json_decode($data);
            if (!isset($data)) {
                throw new Exception("The file is invalid", 0);
            } else {
                return $data;
            }
        }
    }

    /**
     * save data to file, true if success
     *
     * @param $data
     * @return boolean
     */
    public function save($data)
    {
        if ($this->isExist() == false) {
            throw new Exception("file doesn't exist", 0);
        } else {
            $data  = json_encode($data);
            $write = file_put_contents($this->getDir(), $data);
            if ($write == false) {
                throw new Exception("Unable to save data to {$this->name}", 1);
            } else {
                return true;
            }
        }
    }

}
