<?php
namespace App\libs;

use App\libs\Config;
use App\models\Restaurant;
use Exception;

class App extends \Exception
{
    public $restaurant;
    private $restaurant_api_data;
    public $config;
    private $page;
    public function __construct()
    {
        $this->config              = new Config();
        $this->restaurant_api_data = $this->getRestaurantFullInfo();
        $this->restaurant          = new Restaurant($this->restaurant_api_data);
    }

    /**
     * @return Restaurant ID
     */
    public function getRestaurantID()
    {
        return $this->config->get('restaurant_id');
    }

    /**
     * @return default language
     */
    public function getDefaultLanguage()
    {
        return $this->config->get('default_language');
    }


    /**
     * @return default language
     */
    public function getDefaultLanguageBookingForm()
    {
        return $this->config->get('default_booking_form');
    }
    /**
     * @return Base url of application
     */
    public function getBaseURL()
    {
        if ($this->config->get('base_path') == '/') {
            if(empty($_SERVER['REQUEST_SCHEME'])){
                $_SERVER['REQUEST_SCHEME'] = 'http';
            }
            return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
        } else {
            return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . $this->config->get('base_path');
        }
    }

    /**
     * @return Base path of application
     */
    public function getBasePath()
    {
        return $this->config->get('base_path');
    }

    /**
     * @return base api url to retrieve data
     */
    public function getBaseApiURL()
    {
        return $this->config->get('base_api_url');
    }

    /**
     * @return String language_folder in config file
     */
    public function getLanguageFolder()
    {
        return $this->config->get('language_folder');
    }

    /**
     * @return base url to use for book form
     */
    public function getBaseBookURL()
    {
        return $this->config->get('base_book_url');
    }

    /**
     * @return template name for this domain in config
     */
    public function getTemplateName()
    {
        return $this->config->get('template_name');
    }

    /**
     * return customize file for this domain
     *
     * @return string filename
     */
    public function getCustomFile()
    {
        return $this->config->get('customize_filename');
    }

    /**
     * return customize file for this domain
     *
     * @return string filename
     */
    public function getCustomFolder()
    {
        return $this->config->get('customize_foldername');
    }

    /**
     * @return Facebook App ID for this domain in config
     */
    public function getFacebookAppID()
    {
        return $this->config->get('facebook_app_id');
    }

    /**
     * @return string directory uri of  template
     */
    public function getTemplateDirectoryUri()
    {
        return $this->getBaseURL() . '/templates/' . $this->getTemplateName();
    }

    /**
     * Retrieve restaurant data from API
     * @return data of restaurantfullinfo API
     */
    public function getRestaurantFullInfo()
    {
        $url = $this->getBaseApiURL() . '/restaurantfullinfo/' . $this->getRestaurantID();
        $ch  = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if (!strstr($info['http_code'], '2')) {
            // throw new Exception("Can not get data from server", 1);
        } else {
            if (!isset($data)) {
                // throw new Exception("Can not get data from server", 1);
            } else {
                $data = json_decode($data);
                if (!isset($data)) {
                    // throw new Exception("data return from api invalid", 1);
                } else {
                    if ($data->status != 1) {
                        throw new Exception($data->errors, 1);
                    } else {
                        return $data;
                    }
                }
            }
        }
    }

    /**
     * Gets the value of restaurant_api_data.
     *
     * @return mixed
     */
    public function getRestaurantApiData()
    {
        return $this->restaurant_api_data;
    }
}
