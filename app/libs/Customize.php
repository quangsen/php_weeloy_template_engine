<?php
namespace App\libs;

use App\libs\File;

class Customize extends File
{
    private $file;
    public function __construct($template_name, $foldername, $filename)
    {
        $this->setDir(__DIR__ . '/../../public/templates/' . $template_name . '/assets/customize/' . $foldername . '/' . $filename . '.json');
    }

    /**
     * @return String customize css
     */
    public function render()
    {
        try {
            $output = '<style>';
            foreach ($this->read() as $element) {
                $output .= $element->selector . '{';
                foreach ($element->attributes as $key => $value) {
                    $replace_str = $this->transform($value, $element->variables);
                    if ($replace_str != $value) {
                        $output .= $key . ':' . $replace_str . ';';
                    }
                }
                $output .= '}';
            }
            $output .= '</style>';
            return $output;
        } catch (Exception $e) {
            return '';
        }
    }
    public function transform($str, $data)
    {
        foreach ($data as $key => $value) {
            if (isset($value->data) && $value->data != '') {
                if ($value->data == 'none' && $value->type == 'image' && strpos($str, $value->name) > -1) {
                    $str = 'none';
                } else {
                    $str = str_replace($value->name, $value->data, $str);
                }
            }
        }
        return $str;
    }
}
