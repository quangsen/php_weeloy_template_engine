<?php
namespace App\models;

class Other
{
    public $data = [
        'social' => [
            'facebook_page'    => '',
            'instagram_url'    => '',
            'instagram_widget' => '',
        ],
    ];
    public function __construct($data)
    {
        if (isset($data) && isset($data->social)) {
            if (isset($data->social->facebook_page)) {
                $this->data['social']['facebook_page'] = $data->social->facebook_page;
            }
            if (isset($data->social->instagram_url)) {
                $this->data['social']['instagram_url'] = $data->social->instagram_url;
            }
            if (isset($data->social->instagram_widget)) {
                $this->data['social']['instagram_widget'] = $data->social->instagram_widget;
            }
        }
    }

    /**
     * Gets the value of data.
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
