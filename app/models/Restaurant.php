<?php
namespace App\models;

class Restaurant
{
    private $restaurant;
    public function __construct($restaurant)
    {
        $this->restaurant = $restaurant;
    }
    /*
     * Return title of restaurant
     */
    public function getTitle()
    {
        return $this->restaurant->data->restaurantinfo->title;
    }
    /*
     * Return logo image of restaurant
     */
    public function getLogoImage()
    {
        $logo = 'https://media.weeloy.com/upload/restaurant/' . $this->restaurant->data->restaurantinfo->restaurant . '/' . stripslashes($this->restaurant->data->restaurantinfo->logo);
        return $logo;
    }
    /*
     * Return banner image src of restaurant
     */
    public function getBannerImage()
    {
        $banner = 'https://media.weeloy.com/upload/restaurant/' . $this->restaurant->data->restaurantinfo->restaurant . '/1440/' . stripslashes($this->restaurant->data->restaurantinfo->images[0]);
        return $banner;
    }
    /*
     * return list of all images of restaurant
     */
    public function getGallery()
    {
        $images = [];
        foreach ($this->restaurant->data->restaurantinfo->images as $filename) {
            $image = 'https://media.weeloy.com/upload/restaurant/' . $this->restaurant->data->restaurantinfo->restaurant . '/' . stripslashes($this->restaurant->data->restaurantinfo->images[0]);
            array_push($images, $image);
        }
        return $images;
    }
}
