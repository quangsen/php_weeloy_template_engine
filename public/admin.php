<!DOCTYPE html>
<html ng-app="admin">

<head>
    <title>Template Admin</title>
    <?php echo $this->addConfigData(); ?>
        <script type="text/javascript">
        var isLoggedIn = <?php echo $this->auth->isLoggedIn() ? 'true' : 'false'; ?>;
        var user = <?php echo json_encode($this->auth->getUser()); ?>;
        </script>
        <link rel="stylesheet" href="<?php echo $app->getBaseUrl(); ?>/css/tpladmin.min.css">
        <script src="<?php echo $app->getBaseUrl() ?>/js/tpladmin.js"></script>
</head>

<body id="admin-page">
    <header>
        <nav class="navbar navbar-default" ng-show="$state.current.name != 'login'">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" ui-sref="homepage">Template Admin</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Custom <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li ng-repeat="(key, page) in pages" ui-sref-active="active">
                                    <a ui-sref="customize({page: key})" ng-bind="key"></a>
                                </li>
                                <li ui-sref-active="active">
                                    <a ui-sref="customize({page: 'other'})">other</a>
                                </li>
                            </ul>
                        </li>
                        <li ui-sref-active="active">
                            <a ui-sref="language">Language</a>
                        </li>
                        <li ui-sref-active="active">
                            <a ui-sref="social">Social</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown" ng-if="isLoggedIn">
                            <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{user.email}} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a ui-sref="profile">Update password</a></li>
                                <li><a ng-click="logout()">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>

    <body>
        <div class="container-fluid">
            <div ui-view></div>
        </div>
    </body>
</body>

</html>
