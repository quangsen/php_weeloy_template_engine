var Isotope = require('isotope-layout');

(function(app) {
    app.controller('GalleryCtrl', GalleryCtrl);

    GalleryCtrl.$inject = ['$rootScope', '$scope', '$window'];

    function GalleryCtrl($rootScope, $scope, $window) {
        $window.isotope = function() {
            var iso = new Isotope('.isotope-gallery', {
                itemSelector: '.item',
                columnWidth: '.item-size',
                percentPosition: true
            });
        };
    }
})(angular.module('app.components.gallery', []));
