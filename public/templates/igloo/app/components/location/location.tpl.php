<!-- ------------------------ END content location ------------------------------- -->
 <!-- Nội dung phần session -->
      <div id="session_location">
          <h1>Location</h1>
      </div>
      <!-- End Nội dung phần session -->


      

       <!-- Nội dung phần content -->
      <div id="content" class="content_contact_us">
          <div class="container">
              <div class="title_line">
                  <div class="row">
                      <article>
                          <div class="col-xs-12 col-sm-8 col-md-8 contact_us_left" >
                              <div class="col-md-10 form_contact_us">
                                  <div class="maps">
                                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4793427.322858723!2d-83.3268014156132!3d33.94050105323365!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88ffa5f26f4fcf45%3A0xfab58f146ddd1010!2sGreeleyville%2C+SC+29056%2C+Hoa+K%E1%BB%B3!5e0!3m2!1svi!2sus!4v1448960361881" width="650" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                                 </div>
                                 <div class="description_map">
                                     <h3>We’re waiting for you!</h3>
                                     <p>Sed molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin vel ante a orci tempus eleifend ut et magna. Lorem.</p>
                                 </div>
                              </div>
                          </div>
                          <div class="col-xs-12 col-sm-4 col-md-4">
                              <div class="sidebar">
                                  <h3>Working Hours</h3>
                                  <p>Monday to Thursday: 12:00pm – 11:00pm Friday and Saturday: 12:00pm – 1:00am Sunday's we're closed</p>
                                   <h3>Just next to the Eiffel Tower</h3>
                                  <p>Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora.</p>
                                   <h3>How to get there</h3>
                                  <p>Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet arcu. Class aptent taciti sociosqu ad litora.</p>

                              </div>
                             
                          </div>
                      </article>
                  </div>
              </div>
          </div>
      </div>
      <!-- End  content location -->