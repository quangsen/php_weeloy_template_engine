// A utility function to get all app JavaScript sources.
function filterForJS(files, dir) {
    return files.filter(function(file) {
        return (/\.js$/.test(file));
    }).map(function(file) {
        /**
         * @todo : Improve to use Dir param
         */
        return file.replace('public', '');
    });
}

// A utility function to get all app CSS sources.
function filterForCSS(files, dir) {
    return files.filter(function(file) {
        return (/\.css$/.test(file));
    }).map(function(file) {
        /**
         * @todo : Improve to use Dir param
         */
        return file.replace('public', '');
    });
}

// A utility function to get all app CSS sources.
function filterForIndex(files) {
    var indexes = files.filter(function(file) {
        return (/\.html$/.test(file));
    });
    return indexes[0];
}

module.exports = function(grunt) {
    grunt.registerMultiTask('index', 'Process index.html template', function() {
        var dir = this.data.dir;
        var filesSrc = this.filesSrc;

        var indexFileSrc = filterForIndex(filesSrc);
        var cssFiles = filterForCSS(filesSrc, dir);
        var jsFiles = filterForJS(filesSrc, dir);

        var env = this.data.env;
        var config = require('../server/config/config.js')[env];
        var app = {
            VERSION: grunt.config('pkg.version'),
            DEBUG: this.data.debug,
            baseUrl: config.baseUrl,
            socketUri: config.socketUri,
            support_site: config.support_site
        };

        // see - http://gruntjs.com/api/inside-tasks#this.data for documentation
        grunt.file.copy(indexFileSrc, dir + '/index.html', {
            process: function(contents, path) {
                // These are the variables looped over in our index.html exposed as "scripts", "styles", and "version"
                return grunt.template.process(contents, {
                    data: {
                        scripts: jsFiles,
                        styles: cssFiles,
                        author: grunt.config('pkg.author'),
                        date: grunt.template.today("yyyy"),
                        app: app
                    }
                });
            }
        });
    });
};
