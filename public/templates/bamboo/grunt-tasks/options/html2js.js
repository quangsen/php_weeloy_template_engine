module.exports = {
    app: {
        options: {
            module: 'bamboo.template',
        },
        src: [
            'app/**/*.tpl.html'
        ],
        dest: 'app/tpl.js'
    }
};
