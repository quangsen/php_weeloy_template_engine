var path = require('path');

function loadConfig(path) {
    var glob = require('glob');
    var object = {};
    var key;

    glob.sync('*', {
        cwd: path
    }).forEach(function(option) {
        key = option.replace(/\.js$/, '');
        object[key] = require(path + option);
    });
    return object;
}
module.exports = function(grunt) {
    var taskConfig = {
        pkg: grunt.file.readJSON('package.json')
    }

    // Project configuration.
    // grunt.initConfig(taskConfig);

    // Load the plugin that provides the "uglify" task.
    // grunt.loadNpmTasks('grunt-contrib-uglify');

    // Default task(s).
    // grunt.registerTask('default', ['uglify']);

    grunt.util._.extend(taskConfig, loadConfig('./grunt-tasks/options/'));

    grunt.initConfig(taskConfig);

    // 2. Load grunt task modules
    require('load-grunt-tasks')(grunt);

    // 3. Load tasks
    grunt.loadTasks('grunt-tasks');

    // 4. Default task
    grunt.registerTask('default', ['build']);

};
