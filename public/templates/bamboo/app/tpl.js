angular.module('bamboo.template', ['../app/components/about/about.tpl.html', '../app/components/booknow/booknow.tpl.html', '../app/components/chef/chef.tpl.html', '../app/components/contact/contact.tpl.html', '../app/components/gallery/gallery.tpl.html', '../app/components/homepage/homepage.tpl.html', '../app/components/menu/menu.tpl.html', '../app/components/social/social.tpl.html', '../app/shared/partials/restaurant_event.tpl.html']);

angular.module("../app/components/about/about.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/about/about.tpl.html",
    "<div class=\"about\">\n" +
    "    <div class=\"x-main full\" role=\"main\">\n" +
    "        <article id=\"post-2\" class=\"post-2 page type-page status-publish hentry no-post-thumbnail\">\n" +
    "            <div class=\"entry-content content\">\n" +
    "                <div id=\"x-section-1\" class=\"x-section bg-image\" ng-show=\"pages.about.isEnableSection('header')\">\n" +
    "                    <div class=\"x-container\">\n" +
    "                        <div class=\"x-column x-sm x-1-1\">\n" +
    "                            <h2 class=\"h-custom-headline man h2\">\n" +
    "                                <span translate=\"about.headline\" translate-values=\"{restaurant: restaurant}\"></span>\n" +
    "                            </h2>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div id=\"x-section-2\" class=\"x-section\" ng-show=\"pages.about.isEnableSection('content')\">\n" +
    "                    <div class=\"x-container\">\n" +
    "                        <div class=\"x-column x-sm x-1-1\" ng-repeat=\"description in restaurant.getDescription()\">\n" +
    "                            <h2 class=\"h-custom-headline  man h3 accent\"><span ng-bind=\"description.title\"></span></h2>\n" +
    "                            <div class=\"x-raw-content mts mbl\" ng-repeat=\"descBody in description.body\">\n" +
    "                                <p class=\"man\" ng-bind=\"descBody\"></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div id=\"x-section-4\" class=\"x-section bg-image\" ng-show=\"pages.about.isEnableSection('book_button')\">\n" +
    "                    <div class=\"x-container\">\n" +
    "                        <div class=\"x-column x-sm x-1-1\">\n" +
    "                            <div class=\"x-callout cta center-text\">\n" +
    "                                <h2 class=\"h-callout\" translate=\"footer.title\"></h2>\n" +
    "                                <p class=\"p-callout\" translate=\"footer.description\"></p>\n" +
    "                                <a ui-sref=\"book\" class=\"x-btn\" title=\"{{ footer.button | translate }}\" translate=\"footer.button\"></a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </article>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/booknow/booknow.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/booknow/booknow.tpl.html",
    "<div class=\"text-center\">\n" +
    "	<book-iframe restaurant=\"restaurant\"></book-iframe>\n" +
    "</div>");
}]);

angular.module("../app/components/chef/chef.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/chef/chef.tpl.html",
    "<div id=\"chef\">\n" +
    "    <div class=\"col-xs-12\">\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <h1 translate=\"chef.headline\" translate-values=\"{restaurant: restaurant}\"></h1>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12 col-md-3\">\n" +
    "            <div class=\"chef-image text-center\">\n" +
    "                <img ng-src=\"{{ restaurant.chef.getChefImage() }}\" alt=\"{{ restaurant.chef.getChefName() }}\" class=\"img-responsive\">\n" +
    "                <h3 ng-bind=\"restaurant.chef.getChefName()\"></h3>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-12 col-md-9\">\n" +
    "            <div class=\"his-believe\" ng-if=\"restaurant.chef.getChefBelieve() !== ''\">\n" +
    "                <p>\n" +
    "                    <strong ng-if=\"restaurant.chef.isFemale()\" translate=\"chef.her_believe\"></strong>\n" +
    "                    <strong ng-if=\"!restaurant.chef.isFemale()\" translate=\"chef.his_believe\"></strong>\n" +
    "                </p>\n" +
    "                <p ng-bind=\"restaurant.chef.getChefBelieve()\"></p>\n" +
    "            </div>\n" +
    "            <div class=\"his-cuisine\" ng-if=\"restaurant.chef.getChefDescription() !== ''\">\n" +
    "                 <p>\n" +
    "                    <strong ng-if=\"restaurant.chef.isFemale()\" translate=\"chef.her_believe\"></strong>\n" +
    "                    <strong ng-if=\"!restaurant.chef.isFemale()\" translate=\"chef.his_believe\"></strong>\n" +
    "                </p>\n" +
    "                <p ng-bind=\"restaurant.chef.getChefDescription()\"></p>\n" +
    "            </div>\n" +
    "            <div class=\"his-awards\" ng-if=\"restaurant.chef.getChefAward() !== ''\">\n" +
    "                <p>\n" +
    "                    <strong ng-if=\"restaurant.chef.isFemale()\" translate=\"chef.her_believe\"></strong>\n" +
    "                    <strong ng-if=\"!restaurant.chef.isFemale()\" translate=\"chef.his_believe\"></strong>\n" +
    "                </p>\n" +
    "                <p ng-bind=\"restaurant.chef.getChefAward()\"></p>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/contact/contact.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/contact/contact.tpl.html",
    "<div class=\"book\">\n" +
    "    <div class=\"x-main full\" role=\"main\">\n" +
    "        <article id=\"post-205\" class=\"post-205 page type-page status-publish hentry no-post-thumbnail\">\n" +
    "            <div class=\"entry-content content\">\n" +
    "                <div id=\"x-section-1\" class=\"x-section bg-image\" ng-show=\"pages.contact.isEnableSection('header')\">\n" +
    "                    <div class=\"x-container   marginless-columns\" style=\"margin: 0px auto 0px auto; padding: 0px 0px 0px 0px; \">\n" +
    "                        <div class=\"x-column x-sm x-1-2\">\n" +
    "                            <h1 class=\"h-custom-headline left-text man h-intro h2\"><span translate=\"contact.headline\"></span></h1></div>\n" +
    "                        <div class=\"x-column x-sm x-1-2\">\n" +
    "                            <div class=\"x-raw-content hours home\">\n" +
    "                                <table>\n" +
    "                                    <tr class=\"open-hours\">\n" +
    "                                        <td class=\"open-hours-day\">Day</td>\n" +
    "                                        <td class=\"open-hours-lunch\">Lunch</td>\n" +
    "                                        <td class=\"open-hours-dinner\">Dinner</td>\n" +
    "                                    </tr>\n" +
    "                                    <tr class=\"open-hours\" ng-repeat=\"item in restaurant.openhours\">\n" +
    "                                        <td class=\"open-hours-day\" ng-bind=\"item.getDay()\"></td>\n" +
    "                                        <td class=\"open-hours-lunch\" ng-bind=\"item.getLunch()\"></td>\n" +
    "                                        <td class=\"open-hours-dinner\" ng-bind=\"item.getDinner()\"></td>\n" +
    "                                    </tr>\n" +
    "                                </table>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div id=\"x-section-2\" class=\"x-section\" ng-show=\"pages.contact.isEnableSection('map')\">\n" +
    "                    <div class=\"x-container\">\n" +
    "                        <div class=\"x-column x-sm x-1-1\">\n" +
    "                            <div id=\"x-google-map-1\" map restaurant=\"restaurant\"></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div id=\"x-section-3\" class=\"x-section location\" ng-show=\"pages.contact.isEnableSection('address')\">\n" +
    "                    <div class=\"x-container   max width\">\n" +
    "                        <div class=\"x-column x-sm center-text  x-1-3\"><img class=\"x-img x-img-circle\" ng-src=\"{{ restaurant.getLogoImage() }}\" alt=\"{{ restaurant.getTitle() }}\">\n" +
    "                        </div>\n" +
    "                        <div class=\"x-column x-sm x-2-3\">\n" +
    "                            <div class=\"form-group\">\n" +
    "                                <h3 class=\"h-custom-headline left-text h-location h3\"><span ng-bind=\"restaurant.getTitle()\"></span></h3>\n" +
    "                                <div class=\"x-raw-content p-info\">\n" +
    "                                    <p class=\"text-left\"><span ng-bind=\"restaurant.getAddress()\"></span><span ng-if=\"restaurant.getZip() != ''\">, {{restaurant.getZip()}}</span><span ng-if=\"restaurant.getCountry() != ''\">, {{restaurant.getCountry()}}</span></p>\n" +
    "                                </div>\n" +
    "                                <h4 class=\"h-custom-headline left-text h-features h5\"><span translate=\"contact.contact_title\"></span></h4>\n" +
    "                                <div class=\"x-raw-content p-features\">\n" +
    "                                    <p class=\"man\" translate=\"contact.contact_description\"></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                            <div class=\"contact-form\">\n" +
    "                                <form method=\"post\" name=\"ContactForm\" ng-submit=\"ContactForm.$valid && ContactSubmit(contact)\" novalidate>\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <input type=\"text\" name=\"firstname\" ng-model=\"contact.firstname\" placeholder=\"First name\" ng-pattern=\"/^[a-z ,.'-]+$/i\" required>\n" +
    "                                        <div class=\"error\">\n" +
    "                                            <span ng-show=\"ContactForm.$submitted && ContactForm.firstname.$error.required\">Please enter your first name.</span>\n" +
    "                                            <span ng-show=\"ContactForm.$submitted && ContactForm.firstname.$error.pattern\">First name must be alpha only.</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <input type=\"text\" name=\"lastname\" ng-model=\"contact.lastname\" placeholder=\"Last name\" ng-pattern=\"/^[a-z ,.'-]+$/i\" maxlength=\"40\" required>\n" +
    "                                        <div class=\"error\">\n" +
    "                                            <span ng-show=\"ContactForm.$submitted && ContactForm.lastname.$error.required\">Please enter your last name.</span>\n" +
    "                                            <span ng-show=\"ContactForm.$submitted && ContactForm.lastname.$error.pattern\">Last name must be alpha only.</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <input type=\"email\" name=\"email\" ng-model=\"contact.email\" placeholder=\"Email address\" maxlength=\"40\" ng-pattern=\"/^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$/i\" required>\n" +
    "                                        <div class=\"error\">\n" +
    "                                            <span ng-show=\"ContactForm.$submitted && ContactForm.email.$error.required\">Please enter your email.</span>\n" +
    "                                            <span ng-show=\"ContactForm.$submitted && ContactForm.email.$error.pattern\">Please enter a valid email.</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <input type=\"text\" name=\"phone\" ng-model=\"contact.phone\" placeholder=\"Phone\" ng-pattern=\"/^[0-9\\+| ]{6,20}/\" maxlength=\"600\" required>\n" +
    "                                        <div class=\"error\">\n" +
    "                                            <span ng-show=\"ContactForm.$submitted && ContactForm.phone.$error.required\">Please enter your phone number.</span>\n" +
    "                                            <span ng-show=\"ContactForm.$submitted && ContactForm.phone.$error.pattern\">Please enter a valid phone number.</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <textarea ng-model=\"contact.message\" name=\"message\" placeholder=\"Write your message here. We will get back to you within 2 business days.\" required></textarea>\n" +
    "                                        <div class=\"error\">\n" +
    "                                            <span ng-show=\"ContactForm.$sumitted && ContactForm.message.$error.required\">Please enter a message.</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                    <div class=\"form-group\">\n" +
    "                                        <input type=\"submit\" value=\"Submit\">\n" +
    "                                    </div>\n" +
    "                                </form>\n" +
    "                                <div class=\"alert alert-success alert-dismissible\" role=\"alert\" ng-show=\"showSuccessMessage\">\n" +
    "                                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n" +
    "                                    <span>Your message was sent!</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </article>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/gallery/gallery.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/gallery/gallery.tpl.html",
    "<div class=\"gallery\">\n" +
    "    <div class=\"x-main full\" role=\"main\">\n" +
    "        <article id=\"post-68\" class=\"post-68 page type-page status-publish hentry no-post-thumbnail\">\n" +
    "            <div class=\"entry-content content\">\n" +
    "                <div id=\"x-section-1\" class=\"x-section bg-image\" data-x-element=\"section\" data-x-params=\"{&quot;type&quot;:&quot;image&quot;,&quot;parallax&quot;:false}\">\n" +
    "                    <div class=\"x-container\">\n" +
    "                        <div class=\"x-column x-sm x-1-1\">\n" +
    "                            <h1 class=\"h-custom-headline  man h2\"><span translate=\"gallery.headline\"></span></h1>\n" +
    "                            <div class=\"x-raw-content mts\">\n" +
    "                                <p class=\"man\" translate=\"gallery.sub_headline\"></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"list-img\">\n" +
    "                    <div ng-repeat=\"image in restaurant.getGallery()\" class=\"{{ ($index < 2) ? 'first' : ($index < 5) ? 'second' : '' }}\">\n" +
    "                        <a ng-href=\"{{ image.getFullsize() }}\" class=\"fresco\" data-fresco-caption=\"{{ restaurant.getTitle() }} - weeloy.com\" data-fresco-group=\"restau\">\n" +
    "                        <div class=\"col-xs-6 col-md-4\">\n" +
    "                            <img src=\"{{asset('images/gallery.png')}}\" alt=\"\" style=\"background-image:url('{{ image.getThumbnail() }}'); background-position: 50% 50%; background-size:cover; width:100%; \">\n" +
    "                        </div>\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </article>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/homepage/homepage.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/homepage/homepage.tpl.html",
    "<div class=\"slider\" ng-show=\"pages.homepage.isEnableSection('slider')\">\n" +
    "    <!-- <img ng-src=\"{{ restaurant.getBannerImage() }}\" alt=\"\" class=\"img-responsive\"> -->\n" +
    "    <div id=\"home-slider\" class=\"carousel slide\" data-ride=\"carousel\">\n" +
    "        <!-- Wrapper for slides -->\n" +
    "        <div class=\"carousel-inner\" role=\"listbox\">\n" +
    "            <div class=\"item\" ng-repeat=\"image in restaurant.getGallery()\" ng-class=\"{'active':$first}\">\n" +
    "                <img ng-src=\"{{asset('images/slider.png')}}\" alt=\"{{ restaurant.getTitle() }}\" style=\"width: 100%; background-image:url('{{ image.getFullsize() }}'); background-position: 50% 50%; background-size: cover\">\n" +
    "                <div class=\"carousel-caption\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <!-- Controls -->\n" +
    "        <a class=\"left carousel-control\" ng-click=\"SliderControl('prev')\" role=\"button\" data-slide=\"prev\">\n" +
    "            <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>\n" +
    "            <span class=\"sr-only\">Previous</span>\n" +
    "        </a>\n" +
    "        <a class=\"right carousel-control\" ng-click=\"SliderControl('next')\" role=\"button\" data-slide=\"next\">\n" +
    "            <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>\n" +
    "            <span class=\"sr-only\">Next</span>\n" +
    "        </a>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"x-main full\" role=\"main\" id=\"home\">\n" +
    "    <article id=\"post-165\" class=\"post-165 page type-page status-publish hentry no-post-thumbnail\">\n" +
    "        <div class=\"entry-content content\">\n" +
    "            <div id=\"x-section-1\">\n" +
    "                <div class=\"x-container max width\">\n" +
    "                    <div class=\"x-column x-sm x-1-1\" ng-show=\"pages.homepage.isEnableSection('open_hours')\">\n" +
    "                        <h2 class=\"h-custom-headline center-text man h3 accent\"><span>Open 7 Days a Week.</span></h2>\n" +
    "                        <div class=\"x-raw-content hours home\">\n" +
    "                            <table>\n" +
    "                                <tr class=\"open-hours\">\n" +
    "                                    <td class=\"open-hours-day\">Day</td>\n" +
    "                                    <td class=\"open-hours-lunch\">Lunch</td>\n" +
    "                                    <td class=\"open-hours-dinner\">Dinner</td>\n" +
    "                                </tr>\n" +
    "                                <tr class=\"open-hours\" ng-repeat=\"item in restaurant.openhours\">\n" +
    "                                    <td class=\"open-hours-day\" ng-bind=\"item.getDay()\"></td>\n" +
    "                                    <td class=\"open-hours-lunch\" ng-bind=\"item.getLunch()\"></td>\n" +
    "                                    <td class=\"open-hours-dinner\" ng-bind=\"item.getDinner()\"></td>\n" +
    "                                </tr>\n" +
    "                            </table>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div id=\"x-section-2\" class=\"x-section center-text bg-image\" ng-show=\"pages.homepage.isEnableSection('book_button')\">\n" +
    "                <div class=\"x-container max width\">\n" +
    "                    <div class=\"x-column x-sm x-1-1\">\n" +
    "                        <h1 class=\"h-custom-headline  man h2\">\n" +
    "                            <span translate=\"homepage.book_now_title\"></span>\n" +
    "                        </h1>\n" +
    "                        <a class=\"x-btn x-btn-large\" ui-sref=\"book\" title=\"{{ restaurant.book_button.text }}\" ng-bind=\"restaurant.book_button.text\"></a>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div id=\"x-section-4\" class=\"x-section\" ng-show=\"pages.homepage.isEnableSection('events')\">\n" +
    "                <div class=\"x-container center-text  max width\">\n" +
    "                    <div class=\"x-column x-sm x-1-1\">\n" +
    "                        <h2 class=\"h-custom-headline center-text man h3 accent\"><span translate=\"homepage.events_title\" translate-values=\"{restaurant: restaurant}\"></span></h2>\n" +
    "                        <div class=\"x-raw-content mts\">\n" +
    "                            <p class=\"man\" translate=\"homepage.events_title_description\"></p>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"x-container max width\">\n" +
    "                    <div class=\"col-xs-12 col-md-6 event\" ng-repeat=\"event in events\">\n" +
    "                        <div restaurant-event event=\"event\"></div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div id=\"x-section-5\" class=\"x-section\" ng-show=\"pages.homepage.isEnableSection('lasest_news')\">\n" +
    "                <div class=\"x-container center-text  max width\">\n" +
    "                    <div class=\"x-column x-sm x-1-1\">\n" +
    "                        <h2 class=\"h-custom-headline  man h3 accent\"><span>Latest News</span></h2>\n" +
    "                        <div class=\"x-raw-content mts\">\n" +
    "                            <p class=\"man\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium, nisi ut volutpat mollis, leo risus interdum arcu, eget facilisis quam felis id mauris.</p>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"x-container max width\">\n" +
    "                    <div class=\"x-column x-sm x-1-1\">\n" +
    "                        <div class=\"x-recent-posts cf man horizontal\" data-x-element=\"recent_posts\" data-x-params=\"{&quot;fade&quot;:false}\" data-fade=\"false\">\n" +
    "                            <a class=\"x-recent-post3 with-image\" href=\"#\" title=\"Permalink to: &quot;Sushi Newbies: Here’s our Sushi 101 Guide&quot;\">\n" +
    "                                <article id=\"post-8\" class=\"post-8 post type-post status-publish format-standard has-post-thumbnail hentry category-blog\">\n" +
    "                                    <div class=\"entry-wrap\">\n" +
    "                                        <div class=\"x-recent-posts-img\" style=\"background-image: url('{{asset('images/featured-blog-04-1200x688.jpg')}}');\"></div>\n" +
    "                                        <div class=\"x-recent-posts-content\">\n" +
    "                                            <h3 class=\"h-recent-posts\">Sushi Newbies: Here’s our Sushi 101 Guide</h3><span class=\"x-recent-posts-date\">May 11, 2015</span></div>\n" +
    "                                    </div>\n" +
    "                                </article>\n" +
    "                            </a>\n" +
    "                            <a class=\"x-recent-post3 with-image\" href=\"#\" title=\"Permalink to: &quot;Watch Our Top Chef In Action&quot;\">\n" +
    "                                <article id=\"post-10\" class=\"post-10 post type-post status-publish format-standard has-post-thumbnail hentry category-blog\">\n" +
    "                                    <div class=\"entry-wrap\">\n" +
    "                                        <div class=\"x-recent-posts-img\" style=\"background-image: url('{{asset('images/featured-blog-03-1200x688.jpg')}}');\"></div>\n" +
    "                                        <div class=\"x-recent-posts-content\">\n" +
    "                                            <h3 class=\"h-recent-posts\">Watch Our Top Chef In Action</h3><span class=\"x-recent-posts-date\">May 10, 2015</span>\n" +
    "                                        </div>\n" +
    "                                    </div>\n" +
    "                                </article>\n" +
    "                            </a>\n" +
    "                            <a class=\"x-recent-post3 with-image\" href=\"#\" title=\"Permalink to: &quot;Awesome Wine Pairings&quot;\">\n" +
    "                                <article id=\"post-12\" class=\"post-12 post type-post status-publish format-standard has-post-thumbnail hentry category-blog\">\n" +
    "                                    <div class=\"entry-wrap\">\n" +
    "                                        <div class=\"x-recent-posts-img\" style=\"background-image: url('{{asset('images/featured-blog-02-1200x688.jpg')}}');\"></div>\n" +
    "                                        <div class=\"x-recent-posts-content\">\n" +
    "                                            <h3 class=\"h-recent-posts\">Awesome Wine Pairings</h3><span class=\"x-recent-posts-date\">May 9, 2015</span></div>\n" +
    "                                    </div>\n" +
    "                                </article>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </article>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/menu/menu.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/menu/menu.tpl.html",
    "<div class=\"menu-page\">\n" +
    "    <div class=\"x-main full\" role=\"main\">\n" +
    "        <article id=\"post-66\" class=\"post-66 page type-page status-publish hentry no-post-thumbnail\">\n" +
    "            <div class=\"entry-content content\">\n" +
    "                <div id=\"x-section-1\" class=\"x-section bg-image\" ng-show=\"pages.menu.isEnableSection('header')\">\n" +
    "                    <div class=\"x-container\">\n" +
    "                        <div class=\"x-column x-sm   x-1-1\">\n" +
    "                            <h1 class=\"h-custom-headline left-text man h2\"><span translate=\"menu.headline\"></span></h1>\n" +
    "                            <div class=\"x-raw-content mts\">\n" +
    "                                <p class=\"man\" translate=\"menu.sub_headline\"></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div id=\"x-section-2\" class=\"x-section\" ng-repeat=\"menu in restaurant.getMenus()\" ng-show=\"pages.menu.isEnableSection('content')\">\n" +
    "                    <div class=\"x-container center-text  max width\">\n" +
    "                        <div class=\"x-column x-sm x-1-1\">\n" +
    "                            <h2 class=\"h-custom-headline  man h3 accent\">\n" +
    "                                <span ng-bind=\"menu.categorie.value\"></span>\n" +
    "                            </h2>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"x-container marginless-columns\">\n" +
    "                        <div class=\"x-column x-sm x-1-2\">\n" +
    "                            <div class=\"x-raw-content\">\n" +
    "                                <div ng-repeat=\"item in menu.items\" ng-if=\"$odd\">\n" +
    "                                    <h5 class=\"menu-title\"><span class=\"pull-left\" ng-bind=\"item.item_title\"></span><span class=\"pull-right\">{{item.currency}} {{item.price}}</span></h5>\n" +
    "                                    <p ng-bind=\"item.item_description\"></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"x-column x-sm x-1-2\">\n" +
    "                            <div class=\"x-raw-content\">\n" +
    "                                <div class=\"x-raw-content\">\n" +
    "                                        <div ng-repeat=\"item in menu.items\" ng-if=\"$even\">\n" +
    "                                            <h5 class=\"menu-title\"><span class=\"pull-left\" ng-bind=\"item.item_title\"></span><span class=\"pull-right\">{{item.currency}} {{item.price}}</span></h5>\n" +
    "                                    <p ng-bind=\"item.item_description\"></p>\n" +
    "                                        </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div id=\"x-section-5\" class=\"x-section bg-image\" ng-show=\"pages.menu.isEnableSection('book_button')\">\n" +
    "                    <div class=\"x-container\">\n" +
    "                        <div class=\"x-column x-sm x-1-1\">\n" +
    "                            <div class=\"x-callout cta center-text\">\n" +
    "                                <h2 class=\"h-callout\" translate=\"menu.footer_title\"></h2>\n" +
    "                                <p class=\"p-callout\" translate=\"menu.footer_description\"></p>\n" +
    "                                <a ui-sref=\"book\" class=\"x-btn\" translate=\"menu.footer_book_btn\"></a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </article>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/components/social/social.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/social/social.tpl.html",
    "<div id=\"social\" class=\"col-xs-12\">\n" +
    "    <div class=\"col-xs-12 col-md-6\" ng-show=\"pages.social.isEnableSection('facebook')\">\n" +
    "        <h3><a ng-href=\"{{fb_page_url}}\" translate=\"social.find_us_on_facebook\"></a></h3>\n" +
    "        <div class=\"fb-page\" data-href=\"{{fb_page_url}}\" data-tabs=\"timeline\" data-small-header=\"false\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"col-xs-12 col-md-6\" ng-show=\"pages.social.isEnableSection('instagram')\">\n" +
    "        <h3><a href=\"{{ instagram_url }}\" translate=\"social.find_us_on_instagram\"></a></h3>\n" +
    "        <div ng-bind-html=\"instagram_widget | unsafe\"></div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("../app/shared/partials/restaurant_event.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/shared/partials/restaurant_event.tpl.html",
    "<div class=\"x-card-outer mvn flip-from-left center-vertically flip-3d\" data-x-element=\"card\">\n" +
    "    <div class=\"x-card-inner\">\n" +
    "        <div class=\"x-face-outer front\">\n" +
    "            <div class=\"x-face-inner\">\n" +
    "                <div class=\"x-face-content\">\n" +
    "                    <div class=\"x-face-graphic\"><i class=\"fa fa-money\" data-x-icon=\"\"></i></div>\n" +
    "                    <h4 class=\"x-face-title\" translate=\"homepage.event_time\" translate-values=\"{event: event}\"></h4>\n" +
    "                    <p class=\"x-face-text\" ng-bind=\"event.getName()\"></p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"x-face-outer back\">\n" +
    "            <div class=\"x-face-inner\">\n" +
    "                <div class=\"x-face-content\">\n" +
    "                    <h4 class=\"x-face-title\" ng-bind=\"event.getName()\"></h4>\n" +
    "                    <p class=\"x-face-text\" ng-bind=\"event.getDescription()\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);
