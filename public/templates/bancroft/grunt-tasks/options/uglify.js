module.exports = {
    options: {
        //Do not change variables name
        mangle: false
        /**
         * except: ['jQuery', 'angular']
         */
    },
    app: {
        options: {
            sourceMap: false,
        },
        src: ['assets/js/app.js'],
        dest: 'assets/js/app.min.js'
    }
};