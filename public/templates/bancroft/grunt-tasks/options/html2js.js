module.exports = {
    app: {
    	options: {
    		module: 'app.template',
    	},
        src: [
            'app/**/*.tpl.html'
        ],
        dest: 'app/tpl.js'
    }
};
