angular.module('app.template', ['../app/components/homepage/homepage.tpl.html', '../app/shared/partials/menu.tpl.html', '../app/shared/partials/restaurant_event.tpl.html']);

angular.module("../app/components/homepage/homepage.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/components/homepage/homepage.tpl.html",
    "");
}]);

angular.module("../app/shared/partials/menu.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/shared/partials/menu.tpl.html",
    "menu");
}]);

angular.module("../app/shared/partials/restaurant_event.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("../app/shared/partials/restaurant_event.tpl.html",
    "<div class=\"x-card-outer mvn flip-from-left center-vertically flip-3d\" data-x-element=\"card\">\n" +
    "    <div class=\"x-card-inner\">\n" +
    "        <div class=\"x-face-outer front\">\n" +
    "            <div class=\"x-face-inner\">\n" +
    "                <div class=\"x-face-content\">\n" +
    "                    <div class=\"x-face-graphic\"><i class=\"fa fa-money\" data-x-icon=\"\"></i></div>\n" +
    "                    <h4 class=\"x-face-title\" translate=\"homepage.event_time\" translate-values=\"{event: event}\"></h4>\n" +
    "                    <p class=\"x-face-text\" ng-bind=\"event.getName()\"></p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"x-face-outer back\">\n" +
    "            <div class=\"x-face-inner\">\n" +
    "                <div class=\"x-face-content\">\n" +
    "                    <h4 class=\"x-face-title\" ng-bind=\"event.getName()\"></h4>\n" +
    "                    <p class=\"x-face-text\" ng-bind=\"event.getDescription()\">\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);
