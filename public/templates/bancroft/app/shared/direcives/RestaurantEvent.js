(function(app) {
    app.directive('restaurantEvent', theDirective);

    theDirective.$inject = [];

    function theDirective() {
        return {
            restrict: 'AE',
            scope: {
                event: '='
            },
            templateUrl: '../app/shared/partials/restaurant_event.tpl.html',
            link: function(scope, element, attrs) {
                $(element).hover(function() {
                    $(this).addClass('event-hover');
                }, function() {
                    $(this).removeClass('event-hover');
                });
            }
        };
    }
})(angular.module('app.directives.restaurantEvent', []));
